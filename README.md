PushMmall推贴B2B/B2C批零采销订货商城+多商家共享商圈版 V3.2版

#### 介绍
 **Pushmall推贴共享电商营销平台：** 集批零采销订货交易+共享商圈+链盟N+1会员营销+社群团购=S2B2B2C盈利模式。具有平台自营、多商家、供应商入驻、分账账户；一键铺货、一客一价。砍价、秒杀，积分、优惠券、商圈卡、一卡通预充值卡，销售返利奖励等功能。是一款以实体商家为核心电商营销推广平台。uniapp移动端：H5、小程序、APP三合一兼容。 
 **适用场景：** 社区电商、外卖电商。 
 **支持二开开发：** 不加密。
 **
### 新批发   新零售

 **搭建您专属的线上批零采销市场** 
![功能明细图](pushmall-shop/Pushmall%E6%8E%A8%E8%B4%B4%E5%85%B1%E4%BA%AB%E7%94%B5%E5%95%86%E5%B9%B3%E5%8F%B0%E5%8A%9F%E8%83%BD%E5%9B%BE.jpg)
 **是一款为实体店铺电商的营销推广系统** 
![Pushmall推熵商城系统架构图](pushmall-shop/Pushmall%E6%8E%A8%E8%B4%B4%E5%95%86%E5%9F%8E%E7%A4%BA%E6%84%8F%E5%9B%BE.jpg)
 **智能AI数字名片** 
![数字AI名片](pushmall-mp/%E6%99%BA%E8%83%BD%E5%90%8D%E7%89%87.png)
![数字AI名片小程序](pushmall-mp/%E6%8E%A8%E8%B4%B4AI%E5%90%8D%E7%89%87300.jpg)
  ● 整合异业商家资源 获得倍增客源：联合线下优质实体商家，共同打造共享电商，实现客户共享，相互引流，异业联盟收益。从而帮助实体门店降低营销成本，打造私域会员运营体系、提升门店持续盈利能力的同时，鼓励用户线上领券，促进到店消费省钱，分享获益，给消费者带来新一代的消费体； “共创、共赢、共生、共享”互利的商业模式让更多自己认识或不认识的人相互链接起来，互相介绍自己身边有资源有能量的朋友进入商圈，快速整合异业资源扩大交际圈。共享经济使自己闲置的客户资源得以充分利用，大数据使得每次推荐都有迹可循、每次分润都相得益彰、每份付出都有相应回报。

  ● 借助人人可参与的商业模式：PUSHmall推贴共享电商平台以充值钱包为入口，优惠卡券活动、积分活动、邀请新人等活动；做任务让消费者分享所得获取收入等为载体；商家以打造商圈，异业联盟、共享店铺为核心的智能导购营销模式；释放个人商业价值，支持有商服能力的人群实现灵活就业，最终构建一个赋能“实体百业”，共创、共赢、共生、共享的电商服务新生态。 

  ● 共享商圈对于消费者好处：消费者都会去在自己居住或工作地附近的商家消费，去哪个商家消费（吃饭、美容、保健、爱车保养等），经常会碰到选择困难；如果经朋友推荐或分享，手头上又有优惠券，那么对于他选择商铺的时间大大缩短，去消费又可以节省钱。

  ● 分红股东模式：共享店铺就是把你商家闲置的工位、员工、经验、房屋水电等资源，共享给有资源的人，我们把这些人称之为“共享股东”，也称为分红股东，分红股东是指不参与实际经验管理；成为消费分红共享股东，需要投入一定的押金，就可以成为合伙人，共享你的店铺资源。相当于招募了一批给你钱还帮你做销售的高级合伙人。而对于共享股东来说，可以投入少量的押金即可带着自己的资源到别人的店铺里面做自己的生意。

  ● 科学的管理调动员工积极性：其实除了要与商户之间建立起良性的合作，如何调动起店内员工的积极性也是至关重要，因为一线员工是所有理念的最前沿执行者，如何增强员工与“共享商圈”这个理念的黏性，并让员工主动去帮助门店拓客呢？共享商圈模式的系统后台能够解决这些问题；

      ★ 利益捆绑，让员工也能获得分红。
      ★ 极大程度上刺激员工的积极性。
      ★ 解决了客源问题，就可以产生许多良性刺激。
      ★ 门店生意火爆，员工就会有底气有干劲。

  ● 商城平台支持：京东供应链，金蝶、管家婆等上游数据互通；

  ●  **PUSHmall服务型电商平台** ：是蓝领技工服务共享平台。项目以多租户、多商家、师傅端、商家端、用户端、品牌端、门店商城等，以微服务S2B2C模式。聚合水、光、电、建筑建材、维修、安装等多行业，实现供应商+蓝领技工，颠覆传统的自养/雇佣用工方式，解构企业工程/零售用工需求，商家、技术工人、为专业施工团队、用户+供应商企业提供家具建材/家电、机电设备/项目工程“最后一公里”的“同城配送、安装、维修、测量、搬运、配送、配送+安装”等一站式技能服务。

  ●  **PUSHmall服务型电商平台** ：可承接全国工程项目的“到货卸货、分货上楼、专业安装、成品保护、现场清洁、安装自检、验收交付、维修质保”等一站式服务。通过制定规范化施工作业标准，打破工装行业“散、乱、杂、小“的弊端，帮助企业降本增效，重新定义工程安装服务新标准。目前，托管的工程涉及新楼盘精装、城市旧改、酒店、办公楼、厂房、商场、医院、学校、展厅、文娱体育等项目。

        PUSHmall服务型电商平台基于对服务供给资源的共享和服务流程的标准化，帮助师傅用户技能变现，帮助企业用户降本增效，帮助消费者改善服务体验，重新定义大家居行业“最后一公里”服务标准。
                             

 **●  一键呼叫师傅，为您提供安装、维修、跑腿、上门服务。** 

 **●  为有技术、有职能、有交通工具的群体提供灵活就业、灵活创收做任务的平台。** 

 **●  为品牌商家提供安装维修技术工人团队的支持。** ** 

 **     ★ 资源互补共享+区域联合运营

        ★ 家电、家居服务商

        ★ 装修公司、项目经理

        ★ 安装维修团队

        ★ 仓储配送团队

        ★ 大家居创业者

        ★ 品牌商售后服务** 

        ★  **灵活就业、灵活创收：** 为蓝领技术工人、有交通工具、保洁卫生等人群

        ● 服务社区周边：周边1-3公里社区范围生活圈，以平台智能推送服务+跑腿订单配送+异业联盟共享模式，商家门店店长分销制精准裂变等交易模式。最终实现传统行业产业链下沉到终端用户的销售渠道+服务一体的战略，实现千店万商全民皆创业的电商新零售模式。


#### 软件架构
技术选型
* 核心框架：SpringBoot2
* 安全框架：Spring Security+Jwt
* 消息中间件：Redis+RocetMq
* 持久层框架：MyBatis-plus+jpa
* 工具：Lombok+dozer
* 微信开发Java SDK：WxJava
* JDK版本：Java8+
* 数据库：mysql5.7+
* 前端框架：Vue2 + Element-UI+vue-element-admin


#### 版本说明：
分为：商用版、VIP版与免费开源版本

*  **pushmall推贴商城 V2.0免费版** ：只提供VIP版的后端开源代码供学习；
*  **pushmall推贴商城 V2.0正式版** ：VIP付费会员版提供基础版前后端完全开源代码（学习型）；
*  **pushmall推贴批零商城版 V2.8** ：订货商城商用版是具有批零采销的功能模块及场景应用经历，微信支付、支付宝支付、余额预存、对公转账、新增一客一价的场景应用，商品信息单一规格批量上传变为SKU多规格；前后端完全开源代码。(支持供应链数据对接)

*  **pushmall推贴多商家批零采销平台 V3.1**：支持多商家多门店多分仓，支持同城外卖订货模式，平台同时具有批零采销订货版商城的应用场景经历；在批零订货版基础上新增：预售、赠品、特价商品、积分集客、券卡集客模块；前后端完全开源代码。 **上游与第三方商品供应链、ERP进销存财务等数据互通，下游与跑腿物流配送系统数据互通** ；（可定制：一键铺货模块）

*  **pushmall推贴多商家共享订货商城版**  V3.2：多商家共享商圈平台同时具有外卖商城平台的应用场景经历；在订货版基础上新增：预售、赠品、特价商品、积分集客、券卡集客及共享商圈等模块；前后端完全开源代码。

 **SaaS分润分销系统：** SaaS版多租户模式，支持为第三方平台提供分润分销、分账账户+多功能商家收付码。

####  **商用版地址** 
|  **公司官网**  | https://push1688.com/  | --  |  --   |
|---------------|-------------------------|------------|-----------|
| 进销存财务商用体验版| https://erp.push1688.com| -- | -- |
| pushmall推贴自营商城    | https://hr.dsc1688.cn | 用户名：ceshi | 密码：123456 |
| pushmall开源中国博客 | https://my.oschina.net/pushmall | -- | -- |
| pushmall推贴免费版| https://gitee.com/pushmall | 免费VIP版后台 | 完全开源 |
| pushmall推贴商城VIP V2.0版| 手机/微信：13565988178|  **558元** VIP正式版 | 完全开源 |
|  **pushmall商城订货版 V2.8** | 微信：13565988178 | 付费 | 商用付费开源版 |
|  **pushmall推贴多商家商城 V3.0** | 微信：13565988178 | 付费 | 商用付费开源版 |
|  **pushmall推贴同城外卖平台系统 V3.1** | 微信：13565988178 | 付费 | 商用付费开源版 |
|  **pushmall推贴共享电商营销平台 V3.2** | 微信：13565988178 | 付费 | 商用付费开源版 |
|  **pushmall推熵服务型电商平台（微服务版） V4.0** | 微信：13565988178 | 付费 | 商用付费开源版 |
|  **pushmall推贴商城系列开源群** | QQ群号：569330208 | -- |             |

####  **推贴订货商城 V2.0正式版后台前端下载地址** 
https://gitee.com/pushmall/pushmall_foreground?_from=gitee_search

#### 更新计划：

 **pushmall推熵共享电商改版正式版发布** ：

 **2023年 10-11月份优化完成** 

 **1、链动N+1会员营销活动：** 会员信息、会有关系、会员任务、会员类型、基础奖励。

 **2、会员卡：** 重构会员卡、会员年费、商家年费。

 **3、社群团购：** 参考美团社群团购、快团团团购模式。

 **4、链盟2+1会员营销活动：**  优化链盟2+1活动业务流程。

 **5、销售金额返利：** 销售订单根据时间，返利模块。
 
 ** 
### 2023年 12月开发计划
 ** 

 **1、SCRM智能AI数字名片：** 地图位置服务、供求信息、消息。

 **2、智能数字名片：** 会员完善信息，默认生成名片：多个名片、多个企业名片、交换名片、建群名片。

 **3、名片地图：** 根据名片办公地址，就近推荐资源。位置服务导航。

 **4、查资源：** 根据关键字查询就近多少公里范围内的资源，人脉资源、人力资源共享。

 **5、扫码挪车：** 拍照车牌号，虚拟隐私电话，拨打车主电话挪车。免费提供放光静电贴生成二维码
————————————————————————————————————————————————————————————————————————————————————————————————
共享商圈演示：https://adminmerge.dsc1688.com/

开源ERP进销存财务演示版地址：https://erp.push1688.com/

源码托管地址：https://gitee.com/pushmall/pushmall

技术交流QQ群：569330208 ；微信/电话交流：13565988178
——————————————————————————————————————————————————

###  **推贴多商家共享商圈订货平台 V3.2版 功能列表
** ** 
![推贴多商家共享商圈订货平台](pushmall-shop/pushmall%E6%8E%A8%E7%86%B5%E5%85%B1%E4%BA%AB%E7%94%B5%E5%95%86%E7%89%88%20V3.5%E7%89%88_01.jpg))
以上功能模块：支持定制二开，同行价格。说明：另计费用模块单独计算费用
#### 演示小程序与QQ群二维码
![推贴商城JAVA开源QQ群号：569330208](https://images.gitee.com/uploads/images/2021/0202/121743_5123de07_8638338.png "1600531577119159.png")
![数字AI名片](pushmall-mp/%E6%8E%A8%E8%B4%B4AI%E5%90%8D%E7%89%87300.jpg)
![体验版地址](pushmall-shop/%E7%AB%9E%E7%A7%80%E7%BA%B7%E4%BA%AB%E4%BA%8C%E7%BB%B4%E7%A0%81400.png)
#### 订货版商城演示截图
[后台管理]
![商品编辑](pushmall-mp/%E5%95%86%E5%93%81%E4%BF%83%E9%94%80%E7%BC%96%E8%BE%91.png)；
![商品管理](pushmall-mp/%E5%95%86%E5%93%81%E7%AE%A1%E7%90%86.png)；
![预存充值](pushmall-mp/%E5%95%86%E5%AE%B6%E9%A2%84%E5%AD%98%E5%85%85%E5%80%BC%E5%8D%A1.png)；
![积分商城](pushmall-mp/%E5%95%86%E5%AE%B6%E7%A7%AF%E5%88%86%E7%AE%A1%E7%90%86.png)；
![商家管理](pushmall-mp/%E5%95%86%E5%AE%B6%E7%AE%A1%E7%90%86.png)；
![商家活动编辑](pushmall-mp/%E6%B4%BB%E5%8A%A8%E7%BC%96%E8%BE%91%E5%88%B6%E4%BD%9C.png)；
![优惠券制作](pushmall-mp/%E4%BC%98%E6%83%A0%E5%88%B8%E5%88%B6%E4%BD%9C.png)
![共享商圈管理](pushmall-mp/%E5%85%B1%E4%BA%AB%E5%95%86%E5%9C%88.png)；
![个人任务](pushmall-mp/%E4%B8%AA%E4%BA%BA%E7%A7%AF%E5%88%86%E4%BB%BB%E5%8A%A1%E8%AE%BE%E7%BD%AE.png)；
![营销活动](pushmall-mp/%E8%90%A5%E9%94%80%E6%B4%BB%E5%8A%A8.png)；
![一客一价](https://images.gitee.com/uploads/images/2021/0329/135804_247cfa9d_8638338.png "一客一价.png")
![移动端APP](pushmall-mp/%E5%BE%AE%E4%BF%A1%E5%9B%BE%E7%89%87_20230119131736.png)；
![首页](pushmall-mp/%E7%AB%9E%E7%A7%80%E9%A6%96%E9%A1%B5.png)；
![员工角色](pushmall-mp/%E5%91%98%E5%B7%A5%E8%A7%92%E8%89%B2.jpg)；
![员工管理](pushmall-mp/%E5%91%98%E5%B7%A5%E7%AE%A1%E7%90%86.png)；
![扫码支付](pushmall-mp/%E6%89%AB%E7%A0%81%E6%94%AF%E4%BB%98.jpg)；
![商品核销订单](pushmall-mp/%E5%95%86%E5%93%81%E3%80%81%E6%A0%B8%E9%94%80%E8%AE%A2%E5%8D%95.jpg);
![盟主商家分润](pushmall-mp/%E7%9B%9F%E4%B8%BB%E5%95%86%E5%AE%B6%E5%88%86%E6%B6%A6.jpg)；
![活动管理](pushmall-mp/%E6%B4%BB%E5%8A%A8%E7%AE%A1%E7%90%86.jpg)；
![个人商户中心](pushmall-mp/%E6%88%91%E7%9A%84%E4%B8%AD%E5%BF%83.jpg)；
![输入图片说明](pushmall-mp/%E9%A6%96%E9%A1%B5.jpg)；
![会员管理](https://images.gitee.com/uploads/images/2021/0202/123445_838e6fa2_8638338.png "微信图片_20210201153401.png")；
!(https://images.gitee.com/uploads/images/2021/0202/123616_ec8f0b6a_8638338.jpeg "3.jpg")；
![输入图片说明](https://images.gitee.com/uploads/images/2021/0202/123627_79d41320_8638338.jpeg "2.jpg")；
![输入图片说明](https://images.gitee.com/uploads/images/2021/0202/123704_50733d2a_8638338.jpeg "6.jpg")；
![输入图片说明](https://images.gitee.com/uploads/images/2021/0202/123717_264f4842_8638338.jpeg "7.jpg")；
![输入图片说明](https://images.gitee.com/uploads/images/2021/0202/123726_8607bf73_8638338.jpeg "5.jpg")；

#### 技术选型

#### 1 后端使用技术
#### 1.1SpringBoot2
#### 1.2mybatis
#### 1.3SpringSecurity
#### 1.4MyBatis-Plus
#### 1.5Druid
#### 1.6Slf4j
#### 1.7Fastjson
#### 1.8JWT
#### 1.9Redis
#### 1.10Quartz
#### 1.11Mysql
#### 1.12swagger
#### 1.13WxJava
#### 1.14Lombok
#### 1.15Hutool

#### 前端使用技术
#### 2.1 Vue 全家桶
#### 2.2 Element
#### 2.3 uniapp

#### 参与贡献
1.  Fork 本仓库
2.  新建 Feat_xxx 分支
3.  提交代码
4.  新建 Pull Request


####  特别鸣谢
- yshop:https://gitee.com/guchengwuyue/yshopmall
- eladmin:https://github.com/elunez/eladmin
- mybaitsplus:https://github.com/baomidou/mybatis-plus
- hutool:https://github.com/looly/hutool
- wxjava:https://github.com/Wechat-Group/WxJava
- vue:https://github.com/vuejs/vue
- element:https://github.com/ElemeFE/element
