package co.pushmall.mp.service;

import co.pushmall.mp.domain.PushMallWechatTemplate;
import co.pushmall.mp.service.dto.PushMallWechatTemplateDTO;
import co.pushmall.mp.service.dto.PushMallWechatTemplateQueryCriteria;
import org.springframework.data.domain.Pageable;

import java.util.List;
import java.util.Map;

/**
 * @author pushmall
 * @date 2019-12-10
 */
//@CacheConfig(cacheNames = "yxWechatTemplate")
public interface PushMallWechatTemplateService {

    PushMallWechatTemplate findByTempkey(String key);

    /**
     * 查询数据分页
     *
     * @param criteria
     * @param pageable
     * @return
     */
    //@Cacheable
    Map<String, Object> queryAll(PushMallWechatTemplateQueryCriteria criteria, Pageable pageable);

    /**
     * 查询所有数据不分页
     *
     * @param criteria
     * @return
     */
    //@Cacheable
    List<PushMallWechatTemplateDTO> queryAll(PushMallWechatTemplateQueryCriteria criteria);

    /**
     * 根据ID查询
     *
     * @param id
     * @return
     */
    //@Cacheable(key = "#p0")
    PushMallWechatTemplateDTO findById(Integer id);

    /**
     * 创建
     *
     * @param resources
     * @return
     */
    //@CacheEvict(allEntries = true)
    PushMallWechatTemplateDTO create(PushMallWechatTemplate resources);

    /**
     * 编辑
     *
     * @param resources
     */
    //@CacheEvict(allEntries = true)
    void update(PushMallWechatTemplate resources);

    /**
     * 删除
     *
     * @param id
     */
    //@CacheEvict(allEntries = true)
    void delete(Integer id);
}
