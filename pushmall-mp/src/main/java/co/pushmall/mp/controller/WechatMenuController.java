package co.pushmall.mp.controller;


import co.pushmall.exception.BadRequestException;
import co.pushmall.mp.config.WxMpConfiguration;
import co.pushmall.mp.domain.PushMallWechatMenu;
import co.pushmall.mp.service.PushMallWechatMenuService;
import co.pushmall.utils.OrderUtil;
import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import me.chanjar.weixin.common.bean.menu.WxMenu;
import me.chanjar.weixin.common.error.WxErrorException;
import me.chanjar.weixin.mp.api.WxMpService;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @author pushmall
 * @date 2019-10-06
 */
@Api(tags = "商城:微信菜單")
@RestController
@RequestMapping("api")
@SuppressWarnings("unchecked")
public class WechatMenuController {

    private final PushMallWechatMenuService PushMallWechatMenuService;

    public WechatMenuController(PushMallWechatMenuService PushMallWechatMenuService) {
        this.PushMallWechatMenuService = PushMallWechatMenuService;
    }

    @ApiOperation(value = "查询菜单")
    @GetMapping(value = "/PmWechatMenu")
    @PreAuthorize("@el.check('admin','PushMallWechatMenu_ALL','PushMallWechatMenu_SELECT')")
    public ResponseEntity getPushMallWechatMenus() {
        return new ResponseEntity(PushMallWechatMenuService.findById("wechat_menus"), HttpStatus.OK);
    }


    @ApiOperation(value = "创建菜单")
    @PostMapping(value = "/PmWechatMenu")
    @PreAuthorize("@el.check('admin','PushMallWechatMenu_ALL','PushMallWechatMenu_CREATE')")
    public ResponseEntity create(@RequestBody String jsonStr) {
        //if(StrUtil.isNotEmpty("22")) throw new BadRequestException("演示环境禁止操作");
        JSONObject jsonObject = JSON.parseObject(jsonStr);
        String jsonButton = jsonObject.get("buttons").toString();
        PushMallWechatMenu PushMallWechatMenu = new PushMallWechatMenu();
        Boolean isExist = PushMallWechatMenuService.isExist("wechat_menus");
        WxMenu menu = JSONObject.parseObject(jsonStr, WxMenu.class);

        WxMpService wxService = WxMpConfiguration.getWxMpService();
        if (isExist) {
            PushMallWechatMenu.setKey("wechat_menus");
            PushMallWechatMenu.setResult(jsonButton);
            PushMallWechatMenuService.update(PushMallWechatMenu);
        } else {
            PushMallWechatMenu.setKey("wechat_menus");
            PushMallWechatMenu.setResult(jsonButton);
            PushMallWechatMenu.setAddTime(OrderUtil.getSecondTimestampTwo());
            PushMallWechatMenuService.create(PushMallWechatMenu);
        }


        //创建菜单
        try {
            wxService.getMenuService().menuDelete();
            wxService.getMenuService().menuCreate(menu);
        } catch (WxErrorException e) {
            throw new BadRequestException(e.getMessage());
            // e.printStackTrace();
        }

        return new ResponseEntity(HttpStatus.OK);
    }


}
