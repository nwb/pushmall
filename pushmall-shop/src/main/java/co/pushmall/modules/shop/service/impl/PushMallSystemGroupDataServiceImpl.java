package co.pushmall.modules.shop.service.impl;

import co.pushmall.modules.shop.domain.PushMallSystemGroupData;
import co.pushmall.modules.shop.repository.PushMallSystemGroupDataRepository;
import co.pushmall.modules.shop.service.PushMallSystemGroupDataService;
import co.pushmall.modules.shop.service.dto.PushMallSystemGroupDataDTO;
import co.pushmall.modules.shop.service.dto.PushMallSystemGroupDataQueryCriteria;
import co.pushmall.modules.shop.service.mapper.PushMallSystemGroupDataMapper;
import co.pushmall.utils.QueryHelp;
import co.pushmall.utils.ValidationUtil;
import com.alibaba.fastjson.JSON;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;

/**
 * @author pushmall
 * @date 2019-10-18
 */
@Service
@Transactional(propagation = Propagation.SUPPORTS, readOnly = true, rollbackFor = Exception.class)
public class PushMallSystemGroupDataServiceImpl implements PushMallSystemGroupDataService {

    private final PushMallSystemGroupDataRepository pushMallSystemGroupDataRepository;

    private final PushMallSystemGroupDataMapper pushMallSystemGroupDataMapper;

    public PushMallSystemGroupDataServiceImpl(PushMallSystemGroupDataRepository pushMallSystemGroupDataRepository, PushMallSystemGroupDataMapper pushMallSystemGroupDataMapper) {
        this.pushMallSystemGroupDataRepository = pushMallSystemGroupDataRepository;
        this.pushMallSystemGroupDataMapper = pushMallSystemGroupDataMapper;
    }

    @Override
    public Map<String, Object> queryAll(PushMallSystemGroupDataQueryCriteria criteria, Pageable pageable) {
        Page<PushMallSystemGroupData> page = pushMallSystemGroupDataRepository
                .findAll((root, criteriaQuery, criteriaBuilder)
                                -> QueryHelp.getPredicate(root, criteria, criteriaBuilder)
                        , pageable);
        List<PushMallSystemGroupDataDTO> systemGroupDataDTOS = new ArrayList<>();
        for (PushMallSystemGroupData systemGroupData : page.getContent()) {

            PushMallSystemGroupDataDTO systemGroupDataDTO = pushMallSystemGroupDataMapper
                    .toDto(systemGroupData);
            systemGroupDataDTO.setMap(JSON.parseObject(systemGroupData.getValue()));
            systemGroupDataDTOS.add(systemGroupDataDTO);
        }
        Map<String, Object> map = new LinkedHashMap<>(2);
        map.put("content", systemGroupDataDTOS);
        map.put("totalElements", page.getTotalElements());
        return map;
    }

    @Override
    public List<PushMallSystemGroupDataDTO> queryAll(PushMallSystemGroupDataQueryCriteria criteria) {
        return pushMallSystemGroupDataMapper.toDto(pushMallSystemGroupDataRepository.findAll((root, criteriaQuery, criteriaBuilder) -> QueryHelp.getPredicate(root, criteria, criteriaBuilder)));
    }

    @Override
    public PushMallSystemGroupDataDTO findById(Integer id) {
        Optional<PushMallSystemGroupData> yxSystemGroupData = pushMallSystemGroupDataRepository.findById(id);
        ValidationUtil.isNull(yxSystemGroupData, "PushMallSystemGroupData", "id", id);
        return pushMallSystemGroupDataMapper.toDto(yxSystemGroupData.get());
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public PushMallSystemGroupDataDTO create(PushMallSystemGroupData resources) {
        if (resources.getStatus() == null) {
            resources.setStatus(1);
        }
        return pushMallSystemGroupDataMapper.toDto(pushMallSystemGroupDataRepository.save(resources));
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public void update(PushMallSystemGroupData resources) {
        Optional<PushMallSystemGroupData> optionalPushMallSystemGroupData = pushMallSystemGroupDataRepository.findById(resources.getId());
        ValidationUtil.isNull(optionalPushMallSystemGroupData, "PushMallSystemGroupData", "id", resources.getId());
        PushMallSystemGroupData pushMallSystemGroupData = optionalPushMallSystemGroupData.get();
        pushMallSystemGroupData.copy(resources);
        pushMallSystemGroupDataRepository.save(pushMallSystemGroupData);
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public void delete(Integer id) {
        pushMallSystemGroupDataRepository.deleteById(id);
    }
}
