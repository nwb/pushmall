/**
 * Copyright (C) 2018-2019
 * All rights reserved, Designed By www.pushmall.co
 * 注意：
 * 本软件为www.pushmall.co开发研制，未经购买不得使用
 * 购买后可获得全部源代码（禁止转卖、分享、上传到码云、github等开源平台）
 * 一经发现盗用、分享等行为，将追究法律责任，后果自负
 */
package co.pushmall.modules.shop.service.impl;

import cn.hutool.core.util.ObjectUtil;
import co.pushmall.modules.shop.domain.PushMallSystemUserLevel;
import co.pushmall.modules.shop.domain.PushMallUserLevel;
import co.pushmall.modules.shop.repository.PushMallSystemUserLevelRepository;
import co.pushmall.modules.shop.repository.PushMallUserLevelRepository;
import co.pushmall.modules.shop.service.PushMallUserLevelService;
import co.pushmall.modules.shop.service.dto.PushMallSystemUserLevelDTO;
import co.pushmall.modules.shop.service.mapper.PushMallSystemUserLevelMapper;
import co.pushmall.utils.ValidationUtil;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Optional;


/**
 * <p>
 * 用户等级记录表 服务实现类
 * </p>
 *
 * @author pushmall
 * @since 2019-12-06
 */
@Slf4j
@Service
@Transactional(rollbackFor = Exception.class)
public class PushMallUserLevelServiceImpl implements PushMallUserLevelService {

    private final PushMallUserLevelRepository pushMallUserLevelRepository;
    private final PushMallSystemUserLevelRepository pushMallSystemUserLevelRepository;
    private final PushMallSystemUserLevelMapper pushMallSystemUserLevelMapper;

    public PushMallUserLevelServiceImpl(PushMallUserLevelRepository pushMallUserLevelRepository,
                                        PushMallSystemUserLevelRepository pushMallSystemUserLevelRepository,
                                        PushMallSystemUserLevelMapper pushMallSystemUserLevelMapper) {
        this.pushMallUserLevelRepository = pushMallUserLevelRepository;
        this.pushMallSystemUserLevelRepository = pushMallSystemUserLevelRepository;
        this.pushMallSystemUserLevelMapper = pushMallSystemUserLevelMapper;
    }

    /**
     * 设置会员等级
     *
     * @param uid
     * @param levelId
     */
    @Override
    public void setUserLevel(Integer uid, Integer levelId) {
        Optional<PushMallSystemUserLevel> yxSystemUserLevel = pushMallSystemUserLevelRepository.findById(levelId);
        ValidationUtil.isNull(yxSystemUserLevel, "PushMallSystemUserLevel", "id", levelId);
        PushMallSystemUserLevelDTO pushMallSystemUserLevelDTO = pushMallSystemUserLevelMapper.toDto(yxSystemUserLevel.get());

        if (ObjectUtil.isNull(pushMallSystemUserLevelDTO)) {
            return;
        }

        PushMallUserLevel pushMallUserLevel = pushMallUserLevelRepository.selectOne(uid);
        String mark = "恭喜你成为了" + pushMallSystemUserLevelDTO.getName();
        if (ObjectUtil.isNotNull(pushMallUserLevel)) {
            pushMallUserLevelRepository.updateUserLevel(levelId, pushMallSystemUserLevelDTO.getGrade(), (pushMallSystemUserLevelDTO.getDiscount()).intValue(), mark, uid);
        } else {
            return;
        }
    }
}
