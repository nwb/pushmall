package co.pushmall.modules.shop.service.dto;

import lombok.Data;

import java.io.Serializable;


/**
 * @author pushmall
 * @date 2019-10-03
 */
@Data
public class PushMallStoreCategorySmallDTO implements Serializable {

    // 商品分类表ID
    private Integer id;


    // 分类名称
    private String cateName;


}
