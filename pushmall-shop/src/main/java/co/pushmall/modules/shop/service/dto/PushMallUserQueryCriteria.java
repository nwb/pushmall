package co.pushmall.modules.shop.service.dto;

import co.pushmall.annotation.Query;
import lombok.Data;

/**
 * @author pushmall
 * @date 2019-10-06
 */
@Data
public class PushMallUserQueryCriteria {

    // 模糊
    @Query(type = Query.Type.INNER_LIKE)
    private String nickname;

    // 模糊
    @Query(type = Query.Type.INNER_LIKE)
    private String phone;

    @Query
    private Integer isPromoter;

    @Query
    private String userType;
}
