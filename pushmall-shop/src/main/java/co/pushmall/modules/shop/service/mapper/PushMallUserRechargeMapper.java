package co.pushmall.modules.shop.service.mapper;

import co.pushmall.base.BaseMapper;
import co.pushmall.modules.shop.domain.PushMallUserRecharge;
import co.pushmall.modules.shop.service.dto.PushMallUserRechargeDto;
import org.mapstruct.Mapper;
import org.mapstruct.ReportingPolicy;

/**
 * @author pushmall
 * @date 2020-03-02
 */
@Mapper(componentModel = "spring", unmappedTargetPolicy = ReportingPolicy.IGNORE)
public interface PushMallUserRechargeMapper extends BaseMapper<PushMallUserRechargeDto, PushMallUserRecharge> {

}
