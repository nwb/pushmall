package co.pushmall.modules.shop.rest;

import cn.hutool.core.util.ObjectUtil;
import co.pushmall.aop.log.Log;
import co.pushmall.constant.ShopConstants;
import co.pushmall.enums.RedisKeyEnum;
import co.pushmall.modules.shop.domain.PushMallSystemConfig;
import co.pushmall.modules.shop.service.PushMallSystemConfigService;
import co.pushmall.modules.shop.service.dto.PushMallSystemConfigQueryCriteria;
import co.pushmall.mp.config.WxMpConfiguration;
import co.pushmall.mp.config.WxPayConfiguration;
import co.pushmall.utils.RedisUtil;
import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @author pushmall
 * @date 2019-10-10
 */
@Api(tags = "商城:配置管理")
@RestController
@RequestMapping("api")
public class SystemConfigController {

    private final PushMallSystemConfigService pushMallSystemConfigService;

    public SystemConfigController(PushMallSystemConfigService pushMallSystemConfigService) {
        this.pushMallSystemConfigService = pushMallSystemConfigService;
    }

    @Log("查询")
    @ApiOperation(value = "查询")
    @GetMapping(value = "/PmSystemConfig")
    @PreAuthorize("@el.check('admin','YXSYSTEMCONFIG_ALL','YXSYSTEMCONFIG_SELECT')")
    public ResponseEntity getPushMallSystemConfigs(PushMallSystemConfigQueryCriteria criteria, Pageable pageable) {
        return new ResponseEntity(pushMallSystemConfigService.queryAll(criteria, pageable), HttpStatus.OK);
    }

    @Log("新增或修改")
    @ApiOperation(value = "新增或修改")
    @PostMapping(value = "/PmSystemConfig")
    @CacheEvict(cacheNames = ShopConstants.PUSHMALL_REDIS_INDEX_KEY, allEntries = true)
    @PreAuthorize("@el.check('admin','YXSYSTEMCONFIG_ALL','YXSYSTEMCONFIG_CREATE')")
    public ResponseEntity create(@RequestBody String jsonStr) {
        //if(StrUtil.isNotEmpty("22")) throw new BadRequestException("演示环境禁止操作");
        JSONObject jsonObject = JSON.parseObject(jsonStr);
        jsonObject.forEach(
                (key, value) -> {
                    PushMallSystemConfig pushMallSystemConfig = pushMallSystemConfigService.findByKey(key);
                    PushMallSystemConfig pushMallSystemConfigModel = new PushMallSystemConfig();
                    pushMallSystemConfigModel.setMenuName(key);
                    pushMallSystemConfigModel.setValue(value.toString());
                    //重新配置微信相关
                    if (RedisKeyEnum.WECHAT_APPID.getValue().equals(key)) {
                        WxMpConfiguration.removeWxMpService();
                        WxPayConfiguration.removeWxPayService();
                    }
                    if (RedisKeyEnum.WXPAY_MCHID.getValue().equals(key) || RedisKeyEnum.WXAPP_APPID.getValue().equals(key)) {
                        WxPayConfiguration.removeWxPayService();
                    }
                    RedisUtil.set(key, value.toString(), 0);
                    if (ObjectUtil.isNull(pushMallSystemConfig)) {
                        pushMallSystemConfigService.create(pushMallSystemConfigModel);
                    } else {
                        pushMallSystemConfigModel.setId(pushMallSystemConfig.getId());
                        pushMallSystemConfigService.update(pushMallSystemConfigModel);
                    }
                }
        );

        return new ResponseEntity(HttpStatus.CREATED);
    }


}
