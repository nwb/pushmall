package co.pushmall.modules.activity.service;

import co.pushmall.modules.activity.domain.PushMallUserExtract;
import co.pushmall.modules.activity.service.dto.PushMallUserExtractDTO;
import co.pushmall.modules.activity.service.dto.PushMallUserExtractQueryCriteria;
import org.springframework.data.domain.Pageable;

import java.util.List;
import java.util.Map;

/**
 * @author pushmall
 * @date 2019-11-14
 */
//@CacheConfig(cacheNames = "yxUserExtract")
public interface PushMallUserExtractService {

    /**
     * 查询数据分页
     *
     * @param criteria
     * @param pageable
     * @return
     */
    //@Cacheable
    Map<String, Object> queryAll(PushMallUserExtractQueryCriteria criteria, Pageable pageable);

    /**
     * 查询所有数据不分页
     *
     * @param criteria
     * @return
     */
    //@Cacheable
    List<PushMallUserExtractDTO> queryAll(PushMallUserExtractQueryCriteria criteria);

    /**
     * 根据ID查询
     *
     * @param id
     * @return
     */
    //@Cacheable(key = "#p0")
    PushMallUserExtractDTO findById(Integer id);

    /**
     * 创建
     *
     * @param resources
     * @return
     */
    //@CacheEvict(allEntries = true)
    PushMallUserExtractDTO create(PushMallUserExtract resources);

    /**
     * 编辑
     *
     * @param resources
     */
    //@CacheEvict(allEntries = true)
    void update(PushMallUserExtract resources);

    /**
     * 删除
     *
     * @param id
     */
    //@CacheEvict(allEntries = true)
    void delete(Integer id);
}
