package co.pushmall.modules.activity.service.impl;

import co.pushmall.modules.activity.domain.PushMallStoreCouponUser;
import co.pushmall.modules.activity.repository.PushMallStoreCouponUserRepository;
import co.pushmall.modules.activity.service.PushMallStoreCouponUserService;
import co.pushmall.modules.activity.service.dto.PushMallStoreCouponUserDTO;
import co.pushmall.modules.activity.service.dto.PushMallStoreCouponUserQueryCriteria;
import co.pushmall.modules.activity.service.mapper.PushMallStoreCouponUserMapper;
import co.pushmall.modules.shop.service.PushMallUserService;
import co.pushmall.utils.QueryHelp;
import co.pushmall.utils.ValidationUtil;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;

/**
 * @author pushmall
 * @date 2019-11-10
 */
@Service
@Transactional(propagation = Propagation.SUPPORTS, readOnly = true, rollbackFor = Exception.class)
public class PushMallStoreCouponUserServiceImpl implements PushMallStoreCouponUserService {

    private final PushMallStoreCouponUserRepository pushMallStoreCouponUserRepository;

    private final PushMallStoreCouponUserMapper pushMallStoreCouponUserMapper;

    private final PushMallUserService userService;

    public PushMallStoreCouponUserServiceImpl(PushMallStoreCouponUserRepository pushMallStoreCouponUserRepository, PushMallStoreCouponUserMapper pushMallStoreCouponUserMapper, PushMallUserService userService) {
        this.pushMallStoreCouponUserRepository = pushMallStoreCouponUserRepository;
        this.pushMallStoreCouponUserMapper = pushMallStoreCouponUserMapper;
        this.userService = userService;
    }

    @Override
    public Map<String, Object> queryAll(PushMallStoreCouponUserQueryCriteria criteria, Pageable pageable) {
        Page<PushMallStoreCouponUser> page = pushMallStoreCouponUserRepository.
                findAll((root, criteriaQuery, criteriaBuilder)
                        -> QueryHelp.getPredicate(root, criteria, criteriaBuilder), pageable);
        //List<PushMallStoreCouponUserDTO> storeOrderDTOS = new ArrayList<>();
        List<PushMallStoreCouponUserDTO> storeOrderDTOS = pushMallStoreCouponUserMapper
                .toDto(page.getContent());
        for (PushMallStoreCouponUserDTO couponUserDTO : storeOrderDTOS) {
            couponUserDTO.setNickname(userService.findById(couponUserDTO.getUid()).getNickname());
        }
        Map<String, Object> map = new LinkedHashMap<>(2);
        map.put("content", storeOrderDTOS);
        map.put("totalElements", page.getTotalElements());

        return map;
        //return PageUtil.toPage(page.map(pushMallStoreCouponUserMapper::toDto));
    }

    @Override
    public List<PushMallStoreCouponUserDTO> queryAll(PushMallStoreCouponUserQueryCriteria criteria) {
        return pushMallStoreCouponUserMapper.toDto(pushMallStoreCouponUserRepository.findAll((root, criteriaQuery, criteriaBuilder) -> QueryHelp.getPredicate(root, criteria, criteriaBuilder)));
    }

    @Override
    public PushMallStoreCouponUserDTO findById(Integer id) {
        Optional<PushMallStoreCouponUser> yxStoreCouponUser = pushMallStoreCouponUserRepository.findById(id);
        ValidationUtil.isNull(yxStoreCouponUser, "PushMallStoreCouponUser", "id", id);
        return pushMallStoreCouponUserMapper.toDto(yxStoreCouponUser.get());
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public PushMallStoreCouponUserDTO create(PushMallStoreCouponUser resources) {
        return pushMallStoreCouponUserMapper.toDto(pushMallStoreCouponUserRepository.save(resources));
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public void update(PushMallStoreCouponUser resources) {
        Optional<PushMallStoreCouponUser> optionalPushMallStoreCouponUser = pushMallStoreCouponUserRepository.findById(resources.getId());
        ValidationUtil.isNull(optionalPushMallStoreCouponUser, "PushMallStoreCouponUser", "id", resources.getId());
        PushMallStoreCouponUser pushMallStoreCouponUser = optionalPushMallStoreCouponUser.get();
        pushMallStoreCouponUser.copy(resources);
        pushMallStoreCouponUserRepository.save(pushMallStoreCouponUser);
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public void delete(Integer id) {
        pushMallStoreCouponUserRepository.deleteById(id);
    }
}
